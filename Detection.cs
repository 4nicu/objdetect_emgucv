﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Features2D;
using Emgu.CV.Util;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

namespace imgProcess_test
{
    class Detection
    {
        static List<Point> centroids = new List<Point>();
        static List<int> qualities = new List<int>();   
        static List<int> groups = new List<int>();  // keeps track of the total number of intrest points available in each group

        /// <summary>
        /// Scans for and detects any instance of the component on the searching image.
        /// <para>Returns the scoremap and the searching image indicating where the component is located.</para>
        /// </summary>
        /// <param name="img_sample">Sample image of the object.</param>
        /// <param name="img_observed">The searching image.</param>
        /// <param name="time">Processing time in milliseconds.</param>
        /// <param name="valid_matches">Produces a list of coordinates where the detected objects are located.</param>
        /// <param name="ClusterSize">
        /// Maximum distance between an interest point and a centroid that will be included in the cluster.
        /// </param>
        /// <param name="ClusterRadius">
        /// Maximum distance between an interest point and a centroid that will be included in a cluster.
        /// </param>
        /// <param name="QualityThreshold">
        /// Minimum quality value of possible matches between 0 to 255 that will be included when clustering. 
        /// <para>Default = 128</para>
        /// </param>
        /// <param name="areaProjectedRegion">
        /// Minimum acceptable area of the mapped object. (This was introduced to reduce the possibility of false matches due to noise.)
        /// <para>Default = 3000</para>
        /// </param>
        /// <param name="ORBEdgeThreshold">
        /// Edge Threshold for ORB Detector, defined as a measure of how far from the boundary the points should be.
        /// <para>Default = 10</para>
        /// </param>
        /// <returns></returns>
        public static Tuple<Image<Gray, Byte>, Image<Gray, Byte>> ScanForMatches(Image<Gray, Byte> img_sample, Image<Gray, Byte> img_observed, out long time, 
            out List<Point> valid_matches, int ClusterSize = 20, int ClusterRadius = 25, int QualityThreshold = 128, int areaProjectedRegion = 3000,  int ORBEdgeThreshold = 10)
        {
            bool match_found;
            valid_matches = new List<Point>();

            Image<Gray, Byte> img_copy = img_observed.Clone();
            Stopwatch watch = new Stopwatch();
            watch.Start();
            Point interest_point;
            int quality = 0;

            // Container for interest points and their respective matching qualities
            // Values are used for clustering
            List<Point> n_point = new List<Point>();
            List<int> n_quality = new List<int>();

            // Get feature points of sample image using ORB Descriptor
            VectorOfKeyPoint modelKeyPoints = new VectorOfKeyPoint();
            UMat modelDescriptors = new UMat();
            ORBDetector orb = new ORBDetector(500, 1.2F, 8, 4, 0, 2, ORBDetector.ScoreType.Harris, 31, 20);
            orb.DetectAndCompute(img_sample, null, modelKeyPoints, modelDescriptors, false);

            // Common step sizes: 4 to 8 pizels
            int window_size, img_size, step_size;

            // Create empty scoremap
            Image<Gray, Byte> scoremap = new Image<Gray, byte>(img_observed.Width, img_observed.Height, new Gray(0));

            #region Get size
            // Get window size (square)
            if (img_sample.Height > img_sample.Width)
            {
                window_size = img_sample.Width;
            }
            else
            {
                window_size = img_sample.Height;
            }

            // Get image size to set maximum enlargement limit for the sliding window
            if (img_observed.Height > img_observed.Width)
            {
                img_size = img_observed.Height;
            }
            else
            {
                img_size = img_observed.Width;
            }
            #endregion

            step_size = window_size / 4;

            // Sliding window
            while (window_size < img_size)
            {
                for (int y = 0; y < (img_observed.Height - window_size); y += step_size)
                {
                    for (int x = 0; x < (img_observed.Width - window_size); x += step_size)
                    {
                        Rectangle rect = new Rectangle(x, y, window_size, window_size);
                     
                        // Set new ROI for each iteration
                        Image<Gray, Byte> img_empty = img_observed.Clone();
                        img_empty.ROI = rect;

                        // Try to match image for each ROI 
                        // Produces the coordinate where there might be a match and the matching quality
                        // Values near 255 (white) are considered as a good match
                        using (VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch())
                        {
                            match_found = GetMatch(img_sample.Mat, img_empty.Mat, modelKeyPoints, modelDescriptors, matches, 
                                 out interest_point, out quality, QualityThreshold, ORBEdgeThreshold);
                        }

                        // Draw interest points on scoremap. 
                        // Do not map if the interest point is out of bounds or if the quality value is 0.
                        if (match_found && !(interest_point.X + x >= scoremap.Width || interest_point.Y + y >= scoremap.Height))
                        {
                            scoremap[interest_point.Y + y, interest_point.X + x] = new Gray(quality);

                            // If the quality of this point is greater than the specified quality threshold, store this point for clustering.
                            if (quality > QualityThreshold)
                            {
                                n_point.Add(new Point(interest_point.X + x, interest_point.Y + y));
                                n_quality.Add(quality);
                            }
                        }
                    }
                }
                // Increase window size and repeat iteration
                window_size += window_size / 4;
            }
            watch.Stop();
            time = watch.ElapsedMilliseconds;
            Image<Gray, Byte> matched_img = Cluster(img_observed, n_point, n_quality, out valid_matches, ClusterSize, ClusterRadius);

            return new Tuple<Image<Gray, byte>, Image<Gray, byte>>(matched_img, scoremap);
        }

        /// <summary>
        /// Attempts to locate matches on the ROI by matching feature points. Returns false if there are no matches found.
        /// <para> Code reference: http://www.emgu.com/wiki/index.php/SURF_feature_detector_in_CSharp </para>
        /// </summary>
        /// <param name="modelImage">The sample image of the component.</param>
        /// <param name="observedImage">The searching image.</param>
        /// <param name="modelKeyPoints">Keypoints obtained from the sample image.</param>
        /// <param name="modelDescriptors">Descriptors of the sample image.</param>
        /// <param name="matches"></param>
        /// <param name="interest_points">Possible location of a match on the ROI.</param>
        /// <param name="quality">Quality of the possible match.</param>
        /// <param name="areaProjectedRegion">
        /// Minimum acceptable area of the mapped object. (This was introduced to reduce the possibility of false matches due to noise.)
        /// <para>Default = 3000</para>
        /// </param>
        /// <param name="ORBEdgeThresh">
        /// Edge Threshold for ORB Detector, defined as a measure of how far from the boundary the points should be.
        /// <para>Default = 10</para>
        /// </param>
        /// <returns></returns>
        private static bool GetMatch(Mat modelImage, Mat observedImage, VectorOfKeyPoint modelKeyPoints, UMat modelDescriptors, VectorOfVectorOfDMatch matches,
            out Point interest_points, out int quality, int areaProjectedRegion = 3000, int ORBEdgeThresh = 10)
        {
            int k = 2;
            int avg_y, avg_x;
            double uniquenessThreshold = 0.8;

            interest_points = new Point(0, 0);
            quality = 0;

            Mat homography = null;
            Mat mask;
            VectorOfKeyPoint observedKeyPoints = new VectorOfKeyPoint();

            using (UMat uObservedImage = observedImage.ToUMat(AccessType.Read))
            {
                UMat observedDescriptors = new UMat();
                ORBDetector orb = new ORBDetector(500, 1.2F, 8, ORBEdgeThresh, 0, 2, ORBDetector.ScoreType.Harris, 31, 20);
                orb.DetectAndCompute(uObservedImage, null, observedKeyPoints, observedDescriptors, false);

                BFMatcher matcher = new BFMatcher(DistanceType.L2);
                matcher.Add(modelDescriptors);
                matcher.KnnMatch(observedDescriptors, matches, k, null);
                mask = new Mat(matches.Size, 1, DepthType.Cv8U, 1);
                mask.SetTo(new MCvScalar(255));
                Features2DToolbox.VoteForUniqueness(matches, uniquenessThreshold, mask);

                int nonZeroCount = CvInvoke.CountNonZero(mask);
                if (nonZeroCount >= 4)
                {
                    nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints, matches, mask, 1.5, 20);
                    if (nonZeroCount >= 4)
                    {
                        homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints, observedKeyPoints, matches, mask, 2);
                        float ratio_norm = (float)nonZeroCount / (float)modelKeyPoints.Size * 255;
                        quality = 255 - (int)ratio_norm;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            if (homography != null)
            {
                Rectangle rect = new Rectangle(Point.Empty, modelImage.Size);
                PointF[] pts = new PointF[]
                {
                  new PointF(rect.Left, rect.Bottom),
                  new PointF(rect.Right, rect.Bottom),
                  new PointF(rect.Right, rect.Top),
                  new PointF(rect.Left, rect.Top)
                };

                pts = CvInvoke.PerspectiveTransform(pts, homography);
                Point[] points = Array.ConvertAll<PointF, Point>(pts, Point.Round);
                
                using (VectorOfPoint vp = new VectorOfPoint(points))
                {
                    if (CvInvoke.ContourArea(vp) < areaProjectedRegion)
                    {
                        return false;
                    }
                }
                
                avg_y = (points[0].Y + points[1].Y + points[2].Y + points[3].Y) / 4;
                avg_x = (points[0].X + points[1].X + points[2].X + points[3].X) / 4;

                if (avg_y <= 0 || avg_x <= 0)
                {
                    return false;
                }
                else
                {
                    interest_points = new Point(avg_x, avg_y);
                }
            }
            return true;
        }

        /// <summary>
        /// Perform clustering on interest points.
        /// </summary>
        /// <param name="SearchingImage">The image to be searched.</param>
        /// <param name="n_point">List of intrest points.</param>
        /// <param name="n_quality">Qualities of interest points.</param>
        /// <param name="valid_matches">Returns a list of coordinates of valid matches.</param>
        /// <param name="ClusterSize">Minimum acceptable number of points in a cluster for its centroid to pass as a valid match</param>
        /// <param name="ClusterRadius">Maximum distance between an interest point and a centroid that will be included in the cluster</param>
        /// <returns></returns>
        private static Image<Gray, Byte> Cluster(Image<Gray, Byte> SearchingImage, List<Point> n_point, List<int> n_quality, out List<Point> valid_matches,
            int ClusterSize = 20, int ClusterRadius = 25)
        {
            valid_matches = new List<Point>();
            // Randomize the selection order of points.
            var joined = n_point.Zip(n_quality, (x, y) => new { x, y });
            var shuffled = joined.OrderBy(x => Guid.NewGuid()).ToList();
            n_point = shuffled.Select(pair => pair.x).ToList();
            n_quality = shuffled.Select(pair => pair.y).ToList();

            for (int i = 0; i < n_point.Count; i++)
            {
                // For the first iteration, automatically register this particular point as the first group.
                if (centroids.Count == 0)
                {
                    centroids.Add(n_point[i]);
                    qualities.Add(n_quality[i]);
                    groups.Add(0); 
                    groups[0]++;
                }
                else
                {
                    // If this point does not belong to any of the existing groups,
                    if (!GroupPOI(ClusterRadius, n_point[i].X, n_point[i].Y, n_quality[i]))
                    {
                        // Add a new centroid and its quality.
                        centroids.Add(n_point[i]);
                        qualities.Add(n_quality[i]);

                        // Declare a new group
                        groups.Add(0);  
                        groups[centroids.Count - 1]++;  // add 1 count to this new group
                    }
                }
            }
            
            // Draw white circles on the locations of possible matches in the observed image.
            for (int n = 0; n < groups.Count; n++)
            {
                if (groups[n] > ClusterSize)
                {
                    valid_matches.Add(centroids[n]);
                    CvInvoke.Circle(SearchingImage, centroids[n], 10, new MCvScalar(255), 3);
                }
            }
            return SearchingImage;
        }

        /// <summary>
        /// Determines if the new interest point belongs to any of the existing groups. 
        /// <para>If yes, recalculate the weighted centroid of the cluster, add that point into the group and return true.</para>
        /// </summary>
        /// <param name="radius">Maximum distance from the centroid where the point should be located so that it can be accepted as a member of that group.</param>
        /// <param name="x">X-coordinate of this particular point.</param>
        /// <param name="y">Y-coordinate of this particular point.</param>
        /// <param name="quality">Quality of this particular point.</param>
        /// <returns></returns>
        private static bool GroupPOI(int radius, int x, int y, int quality)
        {
            bool within_circle = false;
            for (int i = 0; i < centroids.Count; i++)
            {
                // Check if the distance of the point from the centroid is within the specified radius.
                int distance;
                distance = (int)Math.Sqrt(Math.Pow(x - centroids[i].X, 2) + Math.Pow(y - centroids[i].Y, 2));
                if (radius >= distance)
                {
                    within_circle = true;
                }

                if (within_circle)
                {
                    // Update centroid
                    int temp_x = centroids[i].X * qualities[i];
                    int temp_y = centroids[i].Y * qualities[i];
                    centroids.RemoveAt(i);
                    centroids.Insert(i, new Point(((x * quality + temp_x) / (qualities[i] + quality)), (y * quality + temp_y) / (qualities[i] + quality)));
                    groups[i]++;    // Add a count to this group
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Attenuates image edges by substracting the laplacian of gaussian from the original image.
        /// <para>Returns the enhanced image.</para>
        /// </summary>
        /// <param name="raw_img">Image to process.</param>
        /// <returns></returns>
        public static Image<Gray, byte> AttenuateEdges(Image<Gray, byte> raw_img)
        {
            double sigma = 1.25;
            int kernel = 3;
            
            Image<Gray, byte> laplacian_img = raw_img.Clone();
            raw_img.Mat.ConvertTo(raw_img.Mat, DepthType.Cv32F);
            CvInvoke.Laplacian(raw_img, laplacian_img, DepthType.Cv32F);
            Image<Gray, byte> result_img = raw_img - laplacian_img.SmoothGaussian(kernel, kernel, sigma, sigma);
            result_img.Mat.ConvertTo(result_img.Mat, DepthType.Cv8U);
            laplacian_img.Mat.ConvertTo(laplacian_img.Mat, DepthType.Cv8U);
            
            return result_img;
        }
    }
}