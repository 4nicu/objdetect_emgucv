# Object Detection Using Feature Descriptors

One of the things I’ve worked on during my short time as an intern in ViTrox. This was written with EmguCV in C#.

## What it does in brief:
1. Get features on the sample image.
2. Apply sliding window over the observed image and set new ROI for each iteration.
3. Attempt to locate any matching features.
4. If there is a possible match on a particular point, store its coordinate and matching quality on a scoremap.
5. Repeat iteration while the sliding window is still within the boundaries of the image, enlarge window for each repeat iteration.
6. Perform clustering on the list of possible matches.
7. Filter out false matches by group size.
