﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;

namespace imgProcess_test
{
    class TemplateMatcher
    {
        static Stopwatch sw = new Stopwatch();
        static List<CenterPoint> groups_xy = new List<CenterPoint>();

        public static void ocv_TemplateMatching()
        {
            string path = Assembly.GetEntryAssembly().Location.Replace(Path.GetFileName(Assembly.GetEntryAssembly().Location), "");
            string img_src = path + "images\\1.png";
            string img_temp = path + "images\\Template.png";

            Image<Bgr, byte> source = new Image<Bgr, byte>(img_src); // Image source
            Image<Bgr, byte> template = new Image<Bgr, byte>(img_temp); // Image template
            Image<Bgr, byte> imageToShow = source.Copy();
            Image<Gray, float> grayimg;

            using (Image<Gray, float> result = source.MatchTemplate(template, TemplateMatchingType.CcoeffNormed))
            {
                double[] minValues, maxValues;
                Point[] minLocations, maxLocations;
                result.MinMax(out minValues, out maxValues, out minLocations, out maxLocations);

                grayimg = result.Copy();
                GetPeaks(result, source, template);
            }

            sw.Stop();
            Console.WriteLine("Elapsed = {0}", sw.Elapsed);

            foreach (CenterPoint pt in groups_xy)
            {
                Rectangle match = new Rectangle(pt.x, pt.y, template.Width, template.Height);
                imageToShow.Draw(match, new Bgr(Color.Red), 1);
            }

            CvInvoke.Imshow("result", grayimg);
            CvInvoke.Imshow("win_1", imageToShow); //Show the image
            CvInvoke.WaitKey(0);
        }

        private static void GetPeaks(Image<Gray, float> grayimg, Image<Bgr, byte> source, Image<Bgr, byte> template)
        {
            int radius;
            Image<Bgr, byte> imageToShow = source.Copy();

            #region Get radius
            if (template.Width > template.Height)
            {
                radius = template.Width / 2;
            }
            else
            {
                radius = template.Height / 2;
            }
            #endregion

            for (int y = 0; y < grayimg.Height; y++)
            {
                for (int x = 0; x < grayimg.Width; x++)
                {
                    float a = grayimg.Data[y, x, 0];
                    if (a > 0.89)
                    {
                        #region Grouping
                        if (groups_xy.Count == 0)
                        {
                            groups_xy.Add(new CenterPoint(x, y));
                        }
                        else
                        {
                            if (!GroupPOI(radius, x, y))
                            {
                                groups_xy.Add(new CenterPoint(x, y));
                            }
                        }
                        #endregion
                    }
                }
            }
        }

        private static bool GroupPOI(int radius, int x, int y)
        {
            for (int i = 0; i < groups_xy.Count; i++)
            {
                if (IsWithinSqr(radius, groups_xy[i].x, groups_xy[i].y, x, y))
                {
                    int temp_x = groups_xy[i].x;
                    int temp_y = groups_xy[i].y;
                    groups_xy.RemoveAt(i);
                    groups_xy.Insert(i, new CenterPoint((x + temp_x) / 2, (y + temp_y) / 2));
                    return true;
                }
            }
            return false;
        }

        private static bool IsWithinSqr(int r, int c_x, int c_y, int x, int y)
        {
            if ((x < (c_x + r)) && (x > (c_x - r)) && (y < (c_y + r)) && (y > (c_y - r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public struct CenterPoint
        {
            public int x;
            public int y;

            public CenterPoint(int x, int y)
            {
                this.x = x;
                this.y = y;
            }

        }
    }
}
