﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System.Reflection;
using System.IO;
using System.Collections.Generic;

namespace imgProcess_test
{
    class Program
    {
        // EmguCV Download and Installation
        // http://www.emgu.com/wiki/index.php/Download_And_Installation

        public enum PP
        {
            Thr_Bin,
            Thr_Adapt,
            Flt_Gauss,
            Flt_Mdn,
            Flt_Bilat,
            Hst_Equ,
            Hst_Adapt
        };
        
        static void Main(string[] args)
        {
            long time = 0;
            
            string path = Assembly.GetEntryAssembly().Location.Replace(Path.GetFileName(Assembly.GetEntryAssembly().Location), "");
            string img_temp = path + "components//21.jpg";  // sample image
            string img_src = path + "web//8a.jpg";  // searching image
            Image<Gray, Byte> imtemp = new Image<Gray, byte>(img_temp);
            Image<Gray, Byte> imobsrv = new Image<Gray, byte>(img_src);

            #region Usage for Detection class
            // Enhance images
            imtemp = Detection.AttenuateEdges(imtemp);
            imobsrv = Detection.AttenuateEdges(imobsrv);

            // Method to obtain multiple instances of the same component on an image.
            List<Point> list_of_matches = new List<Point>();    // Container for locations of possible matches on the searching image.
            var result = Detection.ScanForMatches(imtemp, imobsrv, out time, out list_of_matches);
            CvInvoke.Imshow("Matches", result.Item1);
            CvInvoke.Imshow("Scoremap", result.Item2);
            #endregion

            #region Usage for FeatureDescriptor class
            /*
            try
            {
                // Using feature descriptors to find a single instance of a component on the image
                Mat result2 = FeatureDescriptors.Draw(imtemp.Mat, imobsrv.Mat, out time, FeatureDescriptors.DescriptorType.ORB);    // Select between SURF, SIFT or ORB
                CvInvoke.Imshow("Output", result2);
            }
            catch (Exception cv)
            {
                Console.WriteLine(cv.Message);
            }
            */
            #endregion

            #region Usage for TemplateMatcher class
            //TemplateMatcher.ocv_TemplateMatching();
            #endregion

            CvInvoke.WaitKey(0);
            Console.WriteLine("Done");
            Console.ReadKey();
        }
        
        /// <summary>
        /// Function to test for various pre-processing techniques.
        /// </summary>
        /// <param name="what">PP enum</param>
        /// <param name="img">Image to be processed.</param>
        /// <returns></returns>
        static Image<Gray, Byte> ocv_PreProcess(PP what, Image<Gray, Byte> img)
        {
            Image<Gray, Byte> result;

            switch (what)
            {
                case PP.Thr_Bin:
                    result = img.ThresholdBinary(new Gray(45), new Gray(255));
                    break;

                case PP.Thr_Adapt:
                    result = img.ThresholdAdaptive(new Gray(255), AdaptiveThresholdType.GaussianC, ThresholdType.Binary, 13, new Gray(52));
                    break;

                case PP.Flt_Gauss:
                    double sigma = 1.25;
                    int kernel = 3;
                    result = img.SmoothGaussian(kernel, kernel, sigma, sigma);
                    break;

                case PP.Flt_Mdn:
                    result = img.SmoothMedian(3);
                    break;

                case PP.Flt_Bilat:
                    result = img.SmoothBilatral(11, 40, 40);
                    break;

                case PP.Hst_Equ:
                    Image<Gray, Byte> temp = img.Clone();
                    temp._EqualizeHist();
                    result = temp;
                    break;

                case PP.Hst_Adapt:
                    result = img.Clone();
                    CvInvoke.CLAHE(img.Mat, 4, new Size(4, 4), result.Mat);
                    break;

                default:
                    result = img;
                    break;
            }
            
            return result;
        }
    }
}
