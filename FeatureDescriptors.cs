﻿using System;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Features2D;
using Emgu.CV.Util;
using System.Diagnostics;
using Emgu.CV.XFeatures2D;

namespace imgProcess_test
{
    class FeatureDescriptors
    {
        /// Refer at: http://www.emgu.com/wiki/index.php/SURF_feature_detector_in_CSharp
        public enum DescriptorType
        {
            SIFT,
            SURF,
            ORB
        };

        static void FindMatch(Mat modelImage, Mat observedImage, out long matchTime, out VectorOfKeyPoint modelKeyPoints, 
            out VectorOfKeyPoint observedKeyPoints, VectorOfVectorOfDMatch matches, out Mat mask, out Mat homography, 
            DescriptorType type)
        {
            int k = 2;
            double uniquenessThreshold = 0.8;
            double hessianThresh = 200;

            Stopwatch watch;
            homography = null;

            modelKeyPoints = new VectorOfKeyPoint();
            observedKeyPoints = new VectorOfKeyPoint();

            using (UMat uModelImage = modelImage.ToUMat(AccessType.Read))
            using (UMat uObservedImage = observedImage.ToUMat(AccessType.Read))
            {
                UMat modelDescriptors = new UMat();
                UMat observedDescriptors = new UMat();

                #region Select Descriptor
                switch (type)
                {
                    case DescriptorType.SIFT:
                        Console.WriteLine("Using SIFT Descriptor");
                        SIFT sift = new SIFT();
                        sift.DetectAndCompute(uModelImage, null, modelKeyPoints, modelDescriptors, false);
                        sift.DetectAndCompute(uObservedImage, null, observedKeyPoints, observedDescriptors, false);
                        break;

                    case DescriptorType.SURF:
                        Console.WriteLine("Using SURF Descriptor");
                        SURF surfCPU = new SURF(hessianThresh);
                        surfCPU.DetectAndCompute(uModelImage, null, modelKeyPoints, modelDescriptors, false);
                        surfCPU.DetectAndCompute(uObservedImage, null, observedKeyPoints, observedDescriptors, false);
                        break;

                    case DescriptorType.ORB:
                        Console.WriteLine("Using ORB Descriptor");
                        ORBDetector orb = new ORBDetector(500, 1.2F, 8, 6, 0, 2, ORBDetector.ScoreType.Harris, 31, 20);
                        orb.DetectAndCompute(uModelImage, null, modelKeyPoints, modelDescriptors, false);
                        orb.DetectAndCompute(uObservedImage, null, observedKeyPoints, observedDescriptors, false);
                        break;

                    default:
                        break;
                }
                watch = Stopwatch.StartNew();
                #endregion

                BFMatcher matcher = new BFMatcher(DistanceType.L2);
                matcher.Add(modelDescriptors);

                matcher.KnnMatch(observedDescriptors, matches, k, null);
                mask = new Mat(matches.Size, 1, DepthType.Cv8U, 1);
                mask.SetTo(new MCvScalar(255));
                Features2DToolbox.VoteForUniqueness(matches, uniquenessThreshold, mask);
                
                int nonZeroCount = CvInvoke.CountNonZero(mask);
                if (nonZeroCount >= 4)
                {
                    nonZeroCount = Features2DToolbox.VoteForSizeAndOrientation(modelKeyPoints, observedKeyPoints, matches, mask, 1.5, 20);
                    Console.WriteLine("Number of matches:      {0}", nonZeroCount);

                    if (nonZeroCount >= 4)
                        homography = Features2DToolbox.GetHomographyMatrixFromMatchedFeatures(modelKeyPoints,
                           observedKeyPoints, matches, mask, 2);
                }
                
                watch.Stop();
            }
            matchTime = watch.ElapsedMilliseconds;
        }

        public static Mat Draw(Mat modelImage, Mat observedImage, out long matchTime, DescriptorType type)
        {
            Mat homography;
            VectorOfKeyPoint modelKeyPoints;
            VectorOfKeyPoint observedKeyPoints;
            using (VectorOfVectorOfDMatch matches = new VectorOfVectorOfDMatch())
            {
                Mat mask;
                FindMatch(modelImage, observedImage, out matchTime, out modelKeyPoints, out observedKeyPoints, matches,
                   out mask, out homography, type);
                
               if (homography != null)
               {
                   Size size = new Size(homography.Width * 100, homography.Height * 100);//the dst image size,e.g.100x100
                   Mat dst = homography.Clone();//dst image
                   CvInvoke.Resize(homography, dst, size);//resize image
               }
               
               Console.WriteLine("Model Key Points:    {0}", modelKeyPoints.Size);
               Console.WriteLine("Observed Key Points: {0}", observedKeyPoints.Size);
               Console.WriteLine("Matches:             {0}", matches.Size);
               

                //Draw the matched keypoints
                Mat result = new Mat();
                Features2DToolbox.DrawMatches(modelImage, modelKeyPoints, observedImage, observedKeyPoints,
                   matches, result, new MCvScalar(255, 0, 255), new MCvScalar(255, 255, 255), mask);

                #region draw the projected region on the image
                
                if (homography != null)
                {
                    //draw a rectangle along the projected model
                    Rectangle rect = new Rectangle(Point.Empty, modelImage.Size);

                    PointF[] pts = new PointF[]
                    {
                  new PointF(rect.Left, rect.Bottom),
                  new PointF(rect.Right, rect.Bottom),
                  new PointF(rect.Right, rect.Top),
                  new PointF(rect.Left, rect.Top)
                    };
                    
                    pts = CvInvoke.PerspectiveTransform(pts, homography);
                    Point[] points = Array.ConvertAll<PointF, Point>(pts, Point.Round);
                    for (int i = 0; i < points.Length; i++)
                    {
                        Console.WriteLine(points[i]);
                    }
                    
                    using (VectorOfPoint vp = new VectorOfPoint(points))
                    {
                        CvInvoke.Polylines(result, vp, true, new MCvScalar(255, 0, 0, 255), 5);
                        //Console.WriteLine("Contour Area:        {0}", CvInvoke.ContourArea(vp));
                    }
                }
                
                #endregion
                return result;
            }
        }
    }
}
